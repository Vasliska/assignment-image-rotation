#include "image.h"
#include "image_io.h"
#include "image_utils.h"
#include "output.h"

#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern const char* read_mes[];
extern const char* write_mes[];
extern const char * const arg_mes[];

int main(int arg_num, char** args) {
    
    if (arg_num < 3) {
        print_err(arg_mes[ARG_NOT_ENOUGH]);
        return 1;
    }
    if(arg_num > 3){
        print_err(arg_mes[ARG_LOTS]);
        return 1;
    }

    char* src_file = args[1];
    char* dst_file = args[2];

    struct image image = (struct image){0};
    enum image_load_status load_status = image_read_from_bmp(src_file, &image);

    if (load_status == IMAGE_LOAD_OK) {
        print_ok(read_mes[IMAGE_LOAD_OK]);
        struct image* rotated_img = malloc(sizeof(struct image)); 
        if(rotated_img == NULL){
            print_err(read_mes[IMAGE_SOME_PROBLEM]);
            return 1;
        }
        if(!image_rotate(image, rotated_img)){
            print_err(read_mes[IMAGE_SOME_PROBLEM]);
            return 1;
        }
        enum image_save_status save_status = image_save_to_bmp(dst_file, rotated_img);
        free_image_memory(rotated_img);
        free_image_memory(&image);
        if (save_status != IMAGE_SAVE_OK) { 
            print_err(write_mes[save_status]);
            return 1;
        }
        print_ok(write_mes[IMAGE_SAVE_OK]);
        
    } else {
        print_err(read_mes[load_status]); 
        free_image_memory(&image);
        return 1;
    }

    return 0;
}

