#include "image_utils.h"

#include <malloc.h>

bool image_rotate(struct image const img, struct image* rotated_img){	
	
	rotated_img->data_array = malloc(sizeof(struct pixel) * img.height * img.width);
	if(rotated_img->data_array == NULL){
		return false;
	}
	for (size_t i = 0; i < img.height; i++ ) {
		for (size_t j = 0; j < img.width; j++) {
			rotated_img->data_array[j * img.height + (img.height - 1 - i)] = img.data_array[i * img.width + j];
		}
	}
	rotated_img->width = img.height;
	rotated_img->height = img.width;
	return true;
}
