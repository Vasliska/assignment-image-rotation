#include "image_io.h"
#include "image.h"
#include "output.h"

#include <errno.h>
#include <malloc.h>
#include <stdio.h> 
#include <stdlib.h> 


#define BMP_BI_SIZE 40
#define BMP_BIT_COUNT 24
#define BMP_COMPRESSION 0
#define BMP_MAGIC_NUMBER 0x4D42

struct __attribute__((packed)) bmp_file_header {
   
    uint16_t type;
    uint32_t file_size; 
    uint32_t reserved;
    uint32_t data_start_addr;
    uint32_t header_size;
    uint32_t width;
    uint32_t height;
    uint16_t color_planes_num;
    uint16_t bits_per_pixel;
    uint32_t compression_method;
    uint32_t image_size;
    uint32_t x_pixels_per_meter;
    uint32_t y_pixels_per_meter;
    uint32_t colors_num;
    uint32_t important_colors_num;
};

// Запакованная структура, повторяющая один пиксель в bmp-файле.
struct __attribute__((packed)) bmp_pixel_bgr {
    uint8_t b, g, r;
};

static struct bmp_file_header bmp_header_read(FILE* file) {
	struct bmp_file_header header = {0};
	const size_t blocks_count = fread(&header, sizeof(struct bmp_file_header), 1, file);
	if (blocks_count != 1) return (struct bmp_file_header) {0};
	return header;
}

static uint32_t get_padding(uint32_t width){
    return (4 - width * 3 % 4) == 0 ? 0 : 4 - width * 3 % 4;
}


enum image_load_status header_validate(struct bmp_file_header const* header) {
    if (header->bits_per_pixel != BMP_BIT_COUNT 
        || header->file_size !=  header->data_start_addr + header->image_size
        || header->compression_method != BMP_COMPRESSION)
        return IMAGE_HEADER_PROBLEMS; 

    return IMAGE_LOAD_OK;
}

enum image_load_status image_read_from_bmp(char* bmp_file_name, struct image* const dst) {
    FILE* file;
    if(!file_open(&file, bmp_file_name, "rb")){
        return IMAGE_LOAD_OPEN_FAIL;
    }
    struct bmp_file_header header = bmp_header_read(file);
    enum image_load_status header_validation = header_validate(&header);

	if (header_validation != IMAGE_LOAD_OK){
        if(!file_close(file))
            return IMAGE_LOAD_CLOSE_FAIL;
        return header_validation;
    }

    const uint32_t width = header.width;
	const uint32_t height = header.height;
    const uint32_t padding_bytes = get_padding(width);

    if (image_malloc(dst, width, height)) {
        if(!file_close(file))
            return IMAGE_LOAD_CLOSE_FAIL;
    	return IMAGE_SOME_PROBLEM; 
    }
    for (size_t i = 0; i < height; i++) {
        size_t fr = fread(&dst->data_array[i * width], sizeof(struct pixel), width, file);
        if (fr != width || fseek(file, padding_bytes, SEEK_CUR)){  
            if(!file_close(file))
                return IMAGE_LOAD_CLOSE_FAIL;
            return IMAGE_SOME_PROBLEM;
        }
    }
      
    dst->width = width;
    dst->height = height;

    if(!file_close(file))
        return IMAGE_LOAD_CLOSE_FAIL;

    return IMAGE_LOAD_OK;
}

struct bmp_file_header bmp_header_create(uint32_t width, uint32_t height) {
    size_t padding = get_padding(width);
    return (struct bmp_file_header) {
        .type = BMP_MAGIC_NUMBER, 
        .file_size = sizeof(struct bmp_file_header) + padding * height,
        .reserved = 0,    
        .data_start_addr = sizeof(struct bmp_file_header),
        .header_size = BMP_BI_SIZE,    
        .width = width,    
        .height = height,  
        .color_planes_num = 1,  
        .bits_per_pixel = BMP_BIT_COUNT,     
        .compression_method = 0,  
        .image_size = 0,          
        .x_pixels_per_meter = 1,  
        .y_pixels_per_meter = 1, 
        .colors_num = 256*256*256,
        .important_colors_num = 0
    };
}

enum image_save_status image_save_to_bmp(char* bmp_file_name, struct image const* img) {
   
    FILE* file_out;
    if(!file_open(&file_out, bmp_file_name, "wb")){
        return IMAGE_SAVE_OPEN_FAIL;
    }
    
    struct bmp_file_header header = bmp_header_create(img->width, img->height);
    size_t padding = get_padding(img->width);
   
    if (!fwrite(&header, sizeof(struct bmp_file_header), 1, file_out)){
        if(!file_close(file_out))
            return IMAGE_SAVE_CLOSE_FAIL;
        return IMAGE_SAVE_WRITE_FAIL;
    }
    
    for (size_t i = 0; i < img->height; i++) {
        size_t count1=fwrite(&img->data_array[i * img->width], sizeof(struct pixel), img->width, file_out);
        if(count1 != img->width){
            if(!file_close(file_out))
                return IMAGE_SAVE_CLOSE_FAIL;
            return IMAGE_SAVE_WRITE_FAIL;
        }
        if(fseek(file_out, padding, SEEK_CUR)){
            if(!file_close(file_out))
                return IMAGE_SAVE_CLOSE_FAIL;
            return IMAGE_SAVE_WRITE_FAIL;
        }
    }    
    if(!file_close(file_out))
        return IMAGE_SAVE_CLOSE_FAIL;

    return IMAGE_SAVE_OK;
}

bool file_open(FILE** file, const char* name, const char* mode) {
    if (!name) {
        return false;
    }
    *file = fopen(name, mode);
    return file != NULL;
}


bool file_close(FILE* file) {
    return (fclose(file) != EOF);
}
