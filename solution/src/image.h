#ifndef _IMAGE_H
#define _IMAGE_H

#include <stdbool.h>
#include <stdint.h>

struct __attribute__((packed)) image {
    uint32_t width, height;
    struct pixel *data_array;
};

struct pixel {
    uint8_t b, g, r;
};

void free_image_memory(struct image* img);

bool image_malloc(struct image *img,uint32_t width, uint32_t height);
  
#endif
