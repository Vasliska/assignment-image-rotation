#include "image.h"

#include <malloc.h>
#include <stdio.h>
#include <stdlib.h> 
#include <string.h> 

void free_image_memory(struct image* img) {
    if(img)
	    free(img->data_array);
}

bool image_malloc(struct image *img, uint32_t width, uint32_t height){
    if(img){
        img->data_array = malloc(sizeof(struct pixel) * width * height);
        return img->data_array == NULL;
    }
    return true;
}


