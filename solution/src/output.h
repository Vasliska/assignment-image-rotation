#ifndef OUTPUT_H
#define OUTPUT_H

enum image_load_status {
    IMAGE_LOAD_OK = 0,
    IMAGE_LOAD_OPEN_FAIL,
    IMAGE_LOAD_CLOSE_FAIL,
    IMAGE_SOME_PROBLEM,
    IMAGE_HEADER_PROBLEMS
};


enum image_save_status {
    IMAGE_SAVE_OK = 0,
    IMAGE_SAVE_OPEN_FAIL,
    IMAGE_SAVE_WRITE_FAIL,
    IMAGE_SAVE_CLOSE_FAIL
};

enum arg_status{
    ARG_NOT_ENOUGH,
    ARG_LOTS
};

void print_err(const char* error);

void print_ok(const char* text);

#endif
