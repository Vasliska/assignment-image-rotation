#ifndef IMAGE_UTILS
#define IMAGE_UTILS

#include "image.h"

/**
 * Поворачивает картинку на 90 градусов
**/
bool image_rotate(struct image const img, struct image* rotated_img);

#endif
