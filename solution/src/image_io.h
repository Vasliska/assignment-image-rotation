#ifndef _IMAGE_IO
#define _IMAGE_IO

#include "image.h"
#include "output.h"
#include <malloc.h>

/**
 * Читает bmp-файл в указанную ссылку на ссылку на изображение.
 *
**/
enum image_load_status image_read_from_bmp(char* bmp_file_name, struct image* dst);

/**
 * Записывает изображение в файл в формате bmp.
 * 
**/
enum image_save_status image_save_to_bmp(char* bmp_file_name, struct image const* img);

bool file_open(FILE** file, const char* name, const char* mode); 

bool file_close(FILE* file);

#endif

