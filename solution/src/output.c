#include "output.h"
#include <stdio.h>

const char * read_mes[]={
        [IMAGE_LOAD_OK] = "Операция чтения прошла успешно",
        [IMAGE_LOAD_OPEN_FAIL] = "Какие-то проблемы с открытием файла для чтения",
        [IMAGE_LOAD_CLOSE_FAIL] = "траблы с закрытием",
        [IMAGE_SOME_PROBLEM] = "Какие-то проблемы",
        [IMAGE_HEADER_PROBLEMS] = "some problems with header",
};

const char * write_mes[]={
        [IMAGE_SAVE_OK] = "Операция записи прошла успешно",
        [IMAGE_SAVE_OPEN_FAIL] = "Какие-то проблемы с открытием файла для записи",
        [IMAGE_SAVE_WRITE_FAIL] = "Не удaлось записать в файл",
        [IMAGE_SAVE_CLOSE_FAIL] = "problems with close"
};

const char * arg_mes[]={
        [ARG_NOT_ENOUGH] = "Ой ей введите 2 файла",
        [ARG_LOTS] = "Челл куда столько аргументов??"
};
    
void print_err(const char* error){
    fprintf(stderr, "%s", error);
}


void print_ok(const char* text){
    fprintf(stdout, "%s", text);
}
